package com.furymaxim.victoriakotlin.utils

import android.app.Activity
import android.content.Context
import android.preference.PreferenceManager
import android.view.inputmethod.InputMethodManager
import androidx.annotation.NonNull
import androidx.fragment.app.FragmentActivity
import com.furymaxim.victoriakotlin.helpers.TempStorage
import com.furymaxim.victoriakotlin.models.SettingsModel
import com.furymaxim.victoriakotlin.ui.activities.MainActivity
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class Utils {

    companion object {
        fun hideKeyboard(@NonNull activity: Activity) {
            val view = activity.currentFocus
            if (view != null) {
                val inputManager =
                    activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputManager.hideSoftInputFromWindow(
                    view.windowToken,
                    InputMethodManager.HIDE_NOT_ALWAYS
                )
            }
        }

        fun setActionBarTitle(title: String, fragmentActivity: FragmentActivity){
            val actionBar = (fragmentActivity as MainActivity).supportActionBar
            actionBar!!.title = title
        }

        fun getSettings(fragmentActivity: FragmentActivity): MutableList<SettingsModel>? {
            return if(TempStorage.getPreference(fragmentActivity).getString("Settings","") != "") {
                Gson().fromJson(
                    TempStorage.getPreference(fragmentActivity).getString("Settings",""), object : TypeToken<List<SettingsModel>>() {}.type
                )
            } else {
                null
            }
        }

        fun setSettings(fragmentActivity: FragmentActivity, settingsList: MutableList<SettingsModel>?) {
            TempStorage.getEditor(fragmentActivity)
                .putString("Settings", Gson().toJson(settingsList)).apply()
        }
    }
}
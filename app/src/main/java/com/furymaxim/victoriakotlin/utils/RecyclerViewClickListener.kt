package com.furymaxim.victoriakotlin.utils

import android.view.View

interface RecyclerViewClickListener {

    fun onClick(position: Int, view: View)
}
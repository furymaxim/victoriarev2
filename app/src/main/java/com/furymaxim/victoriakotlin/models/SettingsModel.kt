package com.furymaxim.victoriakotlin.models

data class SettingsModel(var name: String?, var topic: String?, var checked: Boolean)
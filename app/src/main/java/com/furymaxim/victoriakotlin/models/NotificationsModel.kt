package com.furymaxim.victoriakotlin.models

data class NotificationsModel(var title: String, var text: String, var date: String)
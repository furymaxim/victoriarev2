package com.furymaxim.victoriakotlin.models

data class BasicItemModel(var title: String, var text: String, var phone: String?)
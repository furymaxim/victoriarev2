package com.furymaxim.victoriakotlin.helpers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateHelper {

    private static final String DEFAULT_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

    private static Date parseTimeUtcToDateCustomPattern(String time, String pattern) {
        try {
            SimpleDateFormat input = new SimpleDateFormat(pattern, Locale.US);
            input.setTimeZone(TimeZone.getTimeZone("UTC"));
            return input.parse(time);
        } catch (Throwable ignored) {
        }
        return null;
    }

    public static Date parseTimeUtcToDate(String time) {
        try {
            SimpleDateFormat input = new SimpleDateFormat(DEFAULT_FORMAT, Locale.US);
            input.setTimeZone(TimeZone.getTimeZone("UTC"));
            return input.parse(time);
        } catch (Throwable ignored) {

        }
        return null;
    }

    private static String formatDate(Date input, String pattern) {
        if (input == null) {
            return null;
        }
        try {
            SimpleDateFormat output = new SimpleDateFormat(pattern, Locale.getDefault());
            return output.format(input);
        } catch (Throwable ignored) {

        }
        return null;
    }

    public static String formatNewsDate(String input) {
        Date date = DateHelper.parseTimeUtcToDateCustomPattern(input, DEFAULT_FORMAT);
        return formatDate(date, "dd MMM yyyy");
    }
}

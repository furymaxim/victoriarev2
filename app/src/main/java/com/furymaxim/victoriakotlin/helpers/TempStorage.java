package com.furymaxim.victoriakotlin.helpers;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.fragment.app.FragmentActivity;

public class TempStorage {

    public static SharedPreferences.Editor getEditor(FragmentActivity appCompatActivity) {
        SharedPreferences sharedPref = getPreference(appCompatActivity);
        return sharedPref.edit();
    }

    public static SharedPreferences.Editor getEditor(SharedPreferences preferences) {
        return preferences.edit();
    }

    public static SharedPreferences getPreference(FragmentActivity appCompatActivity) {
        return appCompatActivity.getSharedPreferences("temp_storage", Context.MODE_PRIVATE);
    }

    public static void putString(SharedPreferences.Editor editor, String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public static void putBoolean(SharedPreferences.Editor editor, String key, boolean value) {
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static void putLong(SharedPreferences.Editor editor, String key, long value) {
        editor.putLong(key, value);
        editor.commit();
    }

    public static void removeString(SharedPreferences.Editor editor, String key) {
        editor.remove(key);
        editor.commit();
    }


    public static String getString(SharedPreferences sharedPref, String key) {
        return sharedPref.getString(key, null);
    }

    public static boolean containsString(SharedPreferences sharedPref, String key) {
        return sharedPref.contains(key);
    }
}
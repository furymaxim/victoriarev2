package com.furymaxim.victoriakotlin.ui.nav_drawer;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.core.content.ContextCompat;

import com.furymaxim.victoriakotlin.R;
import com.mikepenz.fastadapter.utils.ViewHolderFactory;
import com.mikepenz.materialdrawer.model.AbstractBadgeableDrawerItem;
import com.mikepenz.materialdrawer.model.BaseViewHolder;

public class MenuDrawerItem extends AbstractBadgeableDrawerItem<MenuDrawerItem> {

    private int textColor;

    @Override
    public ViewHolderFactory<AbstractBadgeableDrawerItem.ViewHolder> getFactory() {
        return new ItemFactory();
    }

    @Override
    protected void bindViewHelper(BaseViewHolder viewHolder) {
        super.bindViewHelper(viewHolder);

        ((ViewHolder) viewHolder).name.setTextColor(textColor);

    }

    @Override
    @LayoutRes
    public int getLayoutRes() {
        return R.layout.menu_drawer_item;
    }

    public MenuDrawerItem withTextColor(Context context, int id) {
        textColor = ContextCompat.getColor(context, id);
        return this;
    }

    public static class ItemFactory extends AbstractBadgeableDrawerItem.ItemFactory {

        @Override
        public ViewHolder create(View v) {
            return new ViewHolder(v);
        }
    }

    private static class ViewHolder extends AbstractBadgeableDrawerItem.ViewHolder {
        private View badgeContainer;
        private View line;
        private TextView name;

        ViewHolder(View view) {
            super(view);
            this.badgeContainer = view.findViewById(com.mikepenz.materialdrawer.R.id.material_drawer_badge_container);
            this.name = view.findViewById(R.id.material_drawer_name);
        }
    }


}


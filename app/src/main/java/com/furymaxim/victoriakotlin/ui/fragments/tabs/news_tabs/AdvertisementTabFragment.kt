package com.furymaxim.victoriakotlin.ui.fragments.tabs.news_tabs


import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.RecyclerView

import com.furymaxim.victoriakotlin.R
import com.furymaxim.victoriakotlin.adapters.BasicRVAdapter
import com.furymaxim.victoriakotlin.models.BasicItemModel
import com.furymaxim.victoriakotlin.ui.fragments.BaseFragment
import kotlinx.android.synthetic.main.fragment_advertisement_tab.*

class AdvertisementTabFragment : BaseFragment(R.layout.fragment_advertisement_tab) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupRecyclerView(recyclerView)
    }

    override fun setupRecyclerView(recyclerView: RecyclerView) {
        super.setupRecyclerView(recyclerView)
        recyclerView.adapter = BasicRVAdapter(getAdvList())
    }

    private fun getAdvList(): MutableList<BasicItemModel> {
        val settingsList: MutableList<BasicItemModel> = ArrayList()

        settingsList.add(
            BasicItemModel(
                "Уборка 3-го подъезда", "Уборка подъезда с 1-го февраля будет выполняться каждый четный вторник месяца",null
            )
        )

        return settingsList
    }

    companion object {
        fun newInstance() = AdvertisementTabFragment()
    }

}

package com.furymaxim.victoriakotlin.ui.fragments

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.furymaxim.victoriakotlin.R
import com.furymaxim.victoriakotlin.adapters.NotificationsRVAdapter
import com.furymaxim.victoriakotlin.models.NotificationsModel
import com.furymaxim.victoriakotlin.ui.activities.MainActivity
import com.furymaxim.victoriakotlin.utils.RecyclerViewClickListener
import com.furymaxim.victoriakotlin.utils.Utils
import kotlinx.android.synthetic.main.fragment_settings.*
import java.util.ArrayList

class NotificationsFragment : BaseFragment(R.layout.fragment_notifications),
    RecyclerViewClickListener {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Utils.setActionBarTitle(getString(R.string.drawer_item_notifications), activity!!)
        setupRecyclerView(recyclerView)
    }

    override fun setupRecyclerView(recyclerView: RecyclerView) {
        super.setupRecyclerView(recyclerView)
        recyclerView.adapter = NotificationsRVAdapter(getNotificationList(), this)
    }

    private fun getNotificationList(): MutableList<NotificationsModel> {
        val notifList: MutableList<NotificationsModel> = ArrayList()
        val settingsList = Utils.getSettings(activity as MainActivity)
        val checkedSettings: MutableList<String?> = ArrayList()

        if (settingsList != null) {
            for (settings in settingsList) {
                if (settings.checked) {
                    checkedSettings.add(settings.topic)
                }
            }

            for (setting in checkedSettings) {
                when (setting) {
                    "declined_bus" -> addNotifToList(notifList, 1)
                    "road_situation" -> addNotifToList(notifList, 2)
                    "topic_bus_last" -> addNotifToList(notifList, 4)
                    "common" -> addNotifToList(notifList, 4)
                    "stock" -> addNotifToList(notifList, 5)
                }

            }
        }

        return notifList
    }

    private fun addNotifToList(list: MutableList<NotificationsModel>, code: Int) {
        when (code) {
            1 -> list.add(
                NotificationsModel(
                    "Уведомление Тема 1",
                    "Описание уведомления какое-то там просто такое-то здесь",
                    "35 минут назад"
                )
            )
            2 -> list.add(
                NotificationsModel(
                    "Уведомление Тема 2",
                    "Описание уведомления какое-то там просто такое-то здесь",
                    "35 минут назад"
                )
            )
            3 -> list.add(
                NotificationsModel(
                    "Уведомление Тема 3",
                    "Описание уведомления какое-то там просто такое-то здесь",
                    "35 минут назад"
                )
            )
            4 -> list.add(
                NotificationsModel(
                    "Уведомление Тема 4",
                    "Описание уведомления какое-то там просто такое-то здесь",
                    "35 минут назад"
                )
            )
            5 -> list.add(
                NotificationsModel(
                    "Уведомление Тема 5",
                    "Описание уведомления какое-то там просто такое-то здесь",
                    "35 минут назад"
                )
            )
        }
    }

    override fun onClick(position: Int, view: View) {
        Toast.makeText(context, getNotificationList()[position].text, Toast.LENGTH_SHORT).show()
    }

    companion object {
        fun newInstance() = NotificationsFragment()
    }


}

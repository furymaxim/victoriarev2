package com.furymaxim.victoriakotlin.ui.fragments

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.furymaxim.victoriakotlin.R
import com.furymaxim.victoriakotlin.adapters.BasicRVAdapter
import com.furymaxim.victoriakotlin.models.BasicItemModel
import com.furymaxim.victoriakotlin.utils.Utils
import kotlinx.android.synthetic.main.fragment_settings.*

class InfoFragment : BaseFragment(R.layout.fragment_info) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Utils.setActionBarTitle(getString(R.string.drawer_item_callback), activity!!)
        setupRecyclerView(recyclerView)
    }

    override fun setupRecyclerView(recyclerView: RecyclerView) {
        super.setupRecyclerView(recyclerView)
        recyclerView.adapter = BasicRVAdapter(getInfoList())
    }

    private fun getInfoList(): MutableList<BasicItemModel> {
        val infoList: MutableList<BasicItemModel> = ArrayList()

        infoList.add(
            BasicItemModel(
                "Диспетчер",
                "Володина Мария Ивановна\nп. 3, кв. 92",
                "89001258796"
            )
        )
        return infoList
    }

    companion object {
        fun newInstance() = InfoFragment()
    }

}

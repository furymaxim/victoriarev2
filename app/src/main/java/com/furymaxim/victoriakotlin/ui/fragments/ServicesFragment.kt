package com.furymaxim.victoriakotlin.ui.fragments

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.RecyclerView

import com.furymaxim.victoriakotlin.R
import com.furymaxim.victoriakotlin.adapters.BasicRVAdapter
import com.furymaxim.victoriakotlin.models.BasicItemModel
import com.furymaxim.victoriakotlin.utils.Utils
import kotlinx.android.synthetic.main.fragment_settings.*


class ServicesFragment : BaseFragment(R.layout.fragment_services) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Utils.setActionBarTitle(getString(R.string.drawer_item_services), activity!!)
        setupRecyclerView(recyclerView)
    }

    override fun setupRecyclerView(recyclerView: RecyclerView) {
        super.setupRecyclerView(recyclerView)
        recyclerView.adapter = BasicRVAdapter(getServicesList())
    }

    private fun getServicesList(): MutableList<BasicItemModel> {
        val settingsList: MutableList<BasicItemModel> = ArrayList()

        settingsList.add(
            BasicItemModel(
                "Уборка Ваших квартир",
                "Наша компания \"Рога и копыта\" рада предложить свои услуги по уборке Ваших квартир и помещений. Звонить с 11.00 до 21.00 пн-пт. ",
                "89501258796"
            )
        )

        return settingsList
    }

    companion object {
        fun newInstance() = ServicesFragment()
    }

}

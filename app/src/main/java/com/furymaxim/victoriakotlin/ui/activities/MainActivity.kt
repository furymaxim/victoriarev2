package com.furymaxim.victoriakotlin.ui.activities

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.furymaxim.victoriakotlin.R
import com.furymaxim.victoriakotlin.ui.fragments.*
import com.furymaxim.victoriakotlin.ui.nav_drawer.MenuDrawerItem
import com.furymaxim.victoriakotlin.utils.Utils
import com.mikepenz.materialdrawer.Drawer
import com.mikepenz.materialdrawer.DrawerBuilder
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.dialog_layout.*

class MainActivity : AppCompatActivity() {

    private var mFragmentNum = 1
    private lateinit var drawer: Drawer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimary)

        setSupportActionBar(toolbar)
        if (supportActionBar != null) supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        setupDrawer(toolbar)
        setDrawerSelection(savedInstanceState)
    }

    private fun setupDrawer(toolbar: Toolbar) {
        drawer = DrawerBuilder()
            .withActivity(this)
            .withToolbar(toolbar)
            .withHeader(R.layout.nav_header)
            .withSelectedItemByPosition(0)
            .addDrawerItems(
                MenuDrawerItem().withName(R.string.drawer_item_news).withIcon(
                    R.drawable.ic_alarm_new
                ).withTextColor(this, R.color.colorSelection)
                    .withSelectedColor(ContextCompat.getColor(this, R.color.primary)),
                MenuDrawerItem().withName(getString(R.string.drawer_item_parking)).withIcon(
                    R.drawable.ic_film
                ).withTextColor(this, R.color.colorSelection)
                    .withSelectedColor(ContextCompat.getColor(this, R.color.primary)),
                MenuDrawerItem().withName(R.string.drawer_item_services).withIcon(
                    R.drawable.ic_bag_new
                ).withTextColor(this, R.color.colorSelection)
                    .withSelectedColor(ContextCompat.getColor(this, R.color.primary)),
                MenuDrawerItem().withName(R.string.drawer_item_callback).withIcon(
                    R.drawable.ic_note_book
                ).withTextColor(this, R.color.colorSelection)
                    .withSelectedColor(ContextCompat.getColor(this, R.color.primary)),
                MenuDrawerItem().withName(R.string.drawer_item_notifications).withIcon(
                    R.drawable.ic_bell_new
                ).withTextColor(this, R.color.colorSelection)
                    .withSelectedColor(ContextCompat.getColor(this, R.color.primary)),
                MenuDrawerItem().withName(R.string.drawer_item_feedback).withIcon(
                    R.drawable.ic_star_new
                ).withTextColor(this, R.color.colorSelection)
                    .withSelectedColor(ContextCompat.getColor(this, R.color.white)),
                MenuDrawerItem().withName(R.string.drawer_item_settings).withIcon(
                    R.drawable.ic_settings_new
                ).withTextColor(this, R.color.colorSelection)
                    .withSelectedColor(ContextCompat.getColor(this, R.color.primary))
            )

            .withOnDrawerListener(object : Drawer.OnDrawerListener {
                override fun onDrawerSlide(drawerView: View?, slideOffset: Float) {}

                override fun onDrawerClosed(drawerView: View?) {}

                override fun onDrawerOpened(drawerView: View?) {
                    Utils.hideKeyboard(this@MainActivity)
                }
            })
            .withOnDrawerItemClickListener { _, position, _ ->
                setFragment(position)

                return@withOnDrawerItemClickListener false
            }
            .build()
    }

    private fun setDrawerSelection(savedInstanceState: Bundle?) {
        if (savedInstanceState != null && savedInstanceState.containsKey(FRAGMENT_NUM_KEY)) {
            mFragmentNum = savedInstanceState.getInt(FRAGMENT_NUM_KEY, 0)
        }
        drawer.setSelectionAtPosition(mFragmentNum)
    }

    @SuppressLint("InflateParams")
    private fun setFragment(position: Int) {
        var fr: Fragment? = null
        when (position) {
            1 -> {
                fr = NewsFragment.newInstance()
            }
            2 -> {
                fr = ParkingCamerasFragment.newInstance()
            }
            3 -> {
                fr = ServicesFragment.newInstance()
            }
            4 -> {
                fr = InfoFragment.newInstance()
            }
            5 -> {
                fr = NotificationsFragment.newInstance()
            }
            6 -> {
                val mDialogView = LayoutInflater.from(this).inflate(R.layout.dialog_layout, null)
                val mBuilder = AlertDialog.Builder(this)
                    .setView(mDialogView)

                val mAlertDialog = mBuilder.show()

                mAlertDialog.send.setOnClickListener {
                    mAlertDialog.dismiss()
                }

                mAlertDialog.setCanceledOnTouchOutside(true)
            }
            7 -> {
                fr = SettingsFragment.newInstance()
            }
        }

        if (fr != null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, fr)
                .commit()
        }

    }

    override fun onBackPressed() {
        val count = supportFragmentManager.backStackEntryCount

        if (drawer.isDrawerOpen) drawer.closeDrawer()

        if (count == 0) {
            super.onBackPressed()
        } else {
            supportFragmentManager.popBackStack()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt(FRAGMENT_NUM_KEY, mFragmentNum)
        super.onSaveInstanceState(outState)
    }

    companion object {
        const val FRAGMENT_NUM_KEY = "MainActivity:FragmentNum"
    }
}

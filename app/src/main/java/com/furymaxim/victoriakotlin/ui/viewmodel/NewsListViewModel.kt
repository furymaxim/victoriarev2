package com.furymaxim.victoriakotlin.ui.viewmodel

import com.furymaxim.victoriakotlin.api.core.APIInterface
import com.furymaxim.victoriakotlin.api.dto.News
import com.furymaxim.victoriakotlin.helpers.Action
import com.jakewharton.rxrelay2.BehaviorRelay
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers

class NewsListViewModel(private val network: APIInterface) {
    private var loadPage: Int = 0

    private val _items: BehaviorRelay<List<News>> = BehaviorRelay.createDefault(emptyList())
    val items: Observable<List<News>> get() = _items

    private val _hasMoreToLoad: BehaviorRelay<Boolean> = BehaviorRelay.createDefault(true)
    val hasMoreToLoad: Observable<Boolean> = _hasMoreToLoad

    val loadMoreAction: Action<Unit, Unit> = Action.fromSingle(isUserEnabled = hasMoreToLoad) {
        network.getNews(loadPage)
            .map { it.items }
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSuccess(::handleNewItems)
            .map { Unit }
    }

    private fun handleNewItems(items: List<News>) {
        // От апи приходит одно и то же при любом page. Поэтому ломается DiffUtil из-за одинаковых id, список трясёт и колбасит.
        // Решение - делаю fixedItems.
        val existingIds = _items.value!!.map { it.id }.toSet()
        val fixedItems = items.filterNot { existingIds.contains(it.id) }
        if (fixedItems.isEmpty()) {
            _hasMoreToLoad.accept(false)
        } else {
            loadPage += 1
            _items.accept(_items.value!! + fixedItems)

        }
    }
}

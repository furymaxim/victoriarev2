package com.furymaxim.victoriakotlin.ui.fragments.tabs.camera_tabs

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.furymaxim.victoriakotlin.R
import kotlinx.android.synthetic.main.fragment_camera_tab.*

class CameraTabFragment : Fragment(R.layout.fragment_camera_tab) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val bundle = this.arguments!!

        when (bundle.getInt("camera", 0)) {
            1 -> root.background = resources.getDrawable(R.drawable.lion, null)
            2 -> root.background = resources.getDrawable(R.drawable.zayac, null)
            3 -> root.background = resources.getDrawable(R.drawable.tiger, null)
        }
    }

    companion object {
        fun newInstance(camera: Int): CameraTabFragment {
            val args = Bundle()
            args.putInt("camera", camera)
            val fragment = CameraTabFragment()
            fragment.arguments = args
            return fragment
        }

    }
}

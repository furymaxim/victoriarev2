package com.furymaxim.victoriakotlin.ui.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.furymaxim.victoriakotlin.R
import com.furymaxim.victoriakotlin.adapters.SettingsRVAdapter
import com.furymaxim.victoriakotlin.models.SettingsModel
import com.furymaxim.victoriakotlin.utils.Utils
import kotlinx.android.synthetic.main.fragment_settings.*

class SettingsFragment : BaseFragment(R.layout.fragment_settings) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Utils.setActionBarTitle(getString(R.string.drawer_item_settings), activity!!)
        setupRecyclerView(recyclerView)
    }

    override fun setupRecyclerView(recyclerView: RecyclerView) {
        super.setupRecyclerView(recyclerView)
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        recyclerView.adapter =
            SettingsRVAdapter(getSettingList(),  activity as FragmentActivity)
    }

    private fun getSettingList(): MutableList<SettingsModel> {
        var settingsList = Utils.getSettings(activity as FragmentActivity)

        val mTitles = resources.getStringArray(R.array.settings_elements)
        val mTopics = resources.getStringArray(R.array.settings_topics)

        if (settingsList == null || mTopics.size != settingsList.size) {
            settingsList = ArrayList<SettingsModel>().toMutableList()
            for (i in mTopics.indices) {
                settingsList.add(SettingsModel(mTitles[i], mTopics[i], false))
            }

            Utils.setSettings(activity as FragmentActivity, settingsList)
        }

        return settingsList
    }


    companion object {
        fun newInstance() = SettingsFragment()
    }

}

package com.furymaxim.victoriakotlin.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.furymaxim.victoriakotlin.R
import com.furymaxim.victoriakotlin.models.NotificationsModel
import com.furymaxim.victoriakotlin.utils.RecyclerViewClickListener

class NotificationsRVAdapter(
    private val notificationsList: MutableList<NotificationsModel>,
    private var mOnNewsClickListener: RecyclerViewClickListener
) : RecyclerView.Adapter<NotificationsRVAdapter.NotificationsViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): NotificationsViewHolder {
        val v = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.recycler_item_notifications, viewGroup, false)

        return NotificationsViewHolder(v, mOnNewsClickListener)
    }

    override fun onBindViewHolder(viewHolder: NotificationsViewHolder, position: Int) {
        viewHolder.setItem(notificationsList[position])
    }

    override fun getItemCount(): Int {
        return notificationsList.size
    }

    inner class NotificationsViewHolder(
        itemView: View,
        private val mListener: RecyclerViewClickListener
    ) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        private val textView: TextView = itemView.findViewById(R.id.notifText)
        private val titleView: TextView = itemView.findViewById(R.id.notifTitle)
        private val dateView: TextView = itemView.findViewById(R.id.notifDate)

        fun setItem(item: NotificationsModel) {
            textView.text = item.text
            titleView.text = item.title
            dateView.text = item.date
            itemView.setOnClickListener(this)
        }

        override fun onClick(view: View) {
            mListener.onClick(adapterPosition, itemView)
        }

    }

}
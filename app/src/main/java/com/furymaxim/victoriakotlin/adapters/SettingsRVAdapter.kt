package com.furymaxim.victoriakotlin.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Switch
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.furymaxim.victoriakotlin.R
import com.furymaxim.victoriakotlin.models.SettingsModel
import com.furymaxim.victoriakotlin.utils.Utils

class SettingsRVAdapter(private val settingsList: MutableList<SettingsModel>, private val activity: FragmentActivity): RecyclerView.Adapter<SettingsRVAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.recycler_item_settings,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return settingsList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.setItem(settingsList[position], position)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val text = view.findViewById<TextView>(R.id.settText)
        private val switch = view.findViewById<Switch>(R.id.settSwitch)
        private var position: Int? = null

        fun setItem(item: SettingsModel, position: Int) {
            text.text = item.name
            switch.isChecked = item.checked
            this.position = position

            switch.setOnCheckedChangeListener { _, isChecked ->
                setTopicChecked(position, isChecked)
            }
        }

        private fun setTopicChecked(position: Int, subscribe: Boolean) {
            /*if (subscribe) {
                Log.e("Subscribe", settingsList!![position]!!.topic)
                FirebaseMessaging.getInstance()
                    .subscribeToTopic(settingsList[position]!!.topic!!)
            } else {
                Log.e("Unsubscribe", settingsList!![position]!!.topic!!)
                FirebaseMessaging.getInstance()
                    .unsubscribeFromTopic(settingsList[position]!!.topic!!)
            }*/

            settingsList[position].checked = subscribe
            Utils.setSettings(activity, settingsList)
        }

    }
}

package com.furymaxim.victoriakotlin.adapters

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.furymaxim.victoriakotlin.R
import com.furymaxim.victoriakotlin.api.core.NetworkService
import com.furymaxim.victoriakotlin.api.dto.News
import com.furymaxim.victoriakotlin.helpers.DateHelper
import io.github.luizgrp.sectionedrecyclerviewadapter.Section
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters

class NewsItemSection(
    private var items: List<News>,
    private val onItemClick: (News, View) -> Unit
) :
    Section(SectionParameters.builder().itemResourceId(R.layout.item_news).build()) {

    fun updateItems(newItems: List<News>): DiffUtil.DiffResult {
        val oldItems = items
        items = newItems
        return DiffUtil.calculateDiff(object : DiffUtil.Callback() {
            override fun getOldListSize() = oldItems.size
            override fun getNewListSize() = newItems.size

            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
                oldItems[oldItemPosition].id == newItems[newItemPosition].id

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
                oldItems[oldItemPosition] == newItems[newItemPosition]
        }, true)
    }

    override fun getContentItemsTotal() = items.size
    override fun getItemViewHolder(view: View) = NewsItemViewHolder(view)

    override fun onBindItemViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder as NewsItemViewHolder
        holder.configure(items[position], onItemClick)
    }
}

class NewsItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val imageView: ImageView = itemView.findViewById(R.id.imageView)
    private val titleView: TextView = itemView.findViewById(R.id.titleView)
    private val timeView: TextView = itemView.findViewById(R.id.dateView)
    private val textView: TextView = itemView.findViewById(R.id.textView)

    fun configure(news: News, onItemClick: (News, View) -> Unit) {
        itemView.setOnClickListener { onItemClick(news, itemView) }

        titleView.text = news.title
        timeView.text = DateHelper.formatNewsDate(news.date)
        textView.text = news.text
        Glide.with(imageView.context).load(NetworkService.BaseUrl.plus(news.images[0]))
            .into(imageView)
    }
}
package com.furymaxim.victoriakotlin.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.furymaxim.victoriakotlin.R
import com.furymaxim.victoriakotlin.models.BasicItemModel

class BasicRVAdapter(private val list: MutableList<BasicItemModel>) :
    RecyclerView.Adapter<BasicRVAdapter.BasicViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): BasicViewHolder {
        return BasicViewHolder(
            LayoutInflater.from(viewGroup.context).inflate(
                R.layout.recycler_item_basic,
                viewGroup,
                false
            )
        )
    }

    override fun onBindViewHolder(viewHolder: BasicViewHolder, i: Int) {
        viewHolder.setItem(list[i])
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class BasicViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var sTitle: TextView = itemView.findViewById(R.id.servicesTitle)
        var sDesc: TextView = itemView.findViewById(R.id.servicesText)
        var phone: TextView = itemView.findViewById(R.id.servicePhone)

        fun setItem(item: BasicItemModel) {
            sTitle.text = item.title
            sDesc.text = item.text
            if(item.phone != null) {
                phone.text = item.phone
            }
        }


    }

}